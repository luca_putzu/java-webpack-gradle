const path = require('path')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const VueLoaderPlugin = require('vue-loader/lib/plugin')
const WatchLiveReloadPlugin = require('sr-webpack-watch-livereload-plugin')

new HtmlWebpackPlugin();
module.exports = {
    mode: 'development',
    devtool: 'source-map',
    entry: './src/main/js/index.js',
    output: {
        path: path.resolve(__dirname, 'build/inplaceWebapp/resources'),
        filename: 'bundle.js'
    },
    module: {
        rules: [{
            test: /\.vue$/,
            loader: 'vue-loader'
        },
        {
            test: /\.js$/,
            loader: 'babel-loader'
        },
        {
            test: /\.css$/,
            loader: [
                {loader: 'style-loader'},
                {loader: 'css-loader'}
            ]
        },
        // {
        //     test: /\.scss$/,
        //     loader: 'style-loader!css-loader!sass-loader'
        // },
        // {
        //     test: /\.(jpe|jpg|woff|woff2|eot|ttf|svg)(\?.*$|$)/,
        //     loader: 'file-loader',
        //     options: {
        //         name: '[name].[ext]',
        //         outputPath: 'fonts/',
        //         publicPath: 'resources/fonts/'
        //     }
        // }
    ]
    },
    plugins: [
        new HtmlWebpackPlugin({
            title: "Hello, World!",
            filename: path.resolve(__dirname, 'build/inplaceWebapp/index.html')
        }),
        new VueLoaderPlugin(),
        new WatchLiveReloadPlugin({
            files: [
                './src/main/js/**/*.js',
                './src/main/js/**/*.vue'
            ]
        })
    ]
}