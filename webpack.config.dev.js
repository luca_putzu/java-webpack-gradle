const merge = require('webpack-merge')
const common = require('./webpack.config.common.js')

module.exports = merge(common, {
	mode: 'development',
	watch: true,
	watchOptions: {
		ignored: /node_modules/,
		aggregateTimeout: 100
	},
    resolve: {
        alias: {
            vue: 'vue/dist/vue.js'
        }
    }
})