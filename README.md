# java-webpack-gradle

Obiettivo del progetto è quello di separare le problematiche di continous integration
dalla gestione del proprio ambiente di sviluppo

L'infrastruttura utilizzerà gradle per il back-end in java e webpack per il frontend javascript.
Dovrebbe essere possibile (se si ha buona memoria) eseguire/modificare/testare il tutto con un qualsiasi editor 
di testo.

Per eseguire il frontend

    npm start

per il backend

    gradlew runApp

l'ordine di esecuzione è irrilevante.
