// import './style.scss'
import vuetifyCss from 'vuetify/dist/vuetify.min.css'
import '@fortawesome/fontawesome-free/js/all.js'

import Vue from 'vue'
import Vuetify from 'vuetify'

import HomePage from './pages/HomePage.vue'

const mainDiv = document.createElement('div')
mainDiv.setAttribute('id', 'vue')
document.getElementsByTagName('body')[0].prepend(mainDiv)

Vue.use(Vuetify)
Vue.use(Vuetify, {
	iconfont: 'fas'
})

new Vue({
	el: '#vue',
	components: {
		HomePage
	},
	template: '<HomePage/>'
})