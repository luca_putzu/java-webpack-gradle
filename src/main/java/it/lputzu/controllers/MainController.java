package it.lputzu.controllers;

import java.util.ArrayList;
import java.util.List;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/")
public class MainController {

    @GetMapping("/")
    public List<String> helloWorld() {
        System.out.println("di qui ci passo");
        List<String> result = new ArrayList<>();
        result.add("hello");
        result.add("world");
        return result;
    }

}